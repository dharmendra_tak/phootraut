from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time

# This is infinite loop.
while(1):
    options = Options()
    options.headless = True
    driver = webdriver.Chrome("D:/Chirpn/LearningSession/chromedriver.exe", options=options)
    try:
        driver.set_page_load_timeout(10)
        driver.get('https://www.phootra.com/')
        driver.maximize_window()
        time.sleep(1)
        print('Website is Up')
    except:
        print('Website is Down')
    driver.quit()
    time.sleep(2)
