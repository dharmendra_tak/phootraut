import variables as v
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
import openpyxl

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")

# Reading Excel File
data = pd.read_excel(v.data_file, sheet_name=v.sheet_name)
cols = len(data.columns)
rows = len(data.index)
data_dict = data.to_dict()
print("Record to be processed: "+str(rows))

# Object to write data in excel
wb = openpyxl.Workbook()
sheet = wb.active

driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options = chrome_options)   #installs required chrome driver version in realtime
driver.get(v.gst_url)
driver.maximize_window()
time.sleep(1)

#looping to arrange data in usable format
for i in range(rows):
    product = str(data_dict[data.columns[0]][i])
    hsn = str(data_dict[data.columns[1]][i])
    print("Accessing HSN: "+hsn)
    driver.find_element(By.XPATH, '//*[@id="input"]').clear()
    driver.find_element(By.XPATH, '//*[@id="input"]').send_keys(hsn)
    driver.find_element(By.XPATH, '//*[@id="input"]').send_keys(Keys.RETURN)
    time.sleep(0.5)
    gst = driver.find_element(By.XPATH, '//*[@id="__next"]/div/div[5]/div[2]/table/tbody/tr/td[4]').text
    # print("Gst for HSN "+hsn+" is "+gst+"%.")
    
    write_product = sheet.cell(row = i+1, column = 1)
    write_product.value = product
    write_hsn = sheet.cell(row=i+1, column=2)
    write_hsn.value = hsn
    write_gst = sheet.cell(row=i+1, column=3)
    write_gst.value = gst

wb.save("D:\Chirpn\phootra\get_gst\hsn_result.xlsx")

# Closing Browser
print("**** All Records Processed. ****")
driver.close()